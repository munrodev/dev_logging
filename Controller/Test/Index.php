<?php
/**
 * Copyright © 2010-2018 Epicor Software Corporation: All Rights Reserved
 */

namespace Dev\LogData\Controller\Test;



class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        echo 'testing 1234';
    }
}
