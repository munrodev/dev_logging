<?php
/**
 * Copyright © 2010-2018 Epicor Software Corporation: All Rights Reserved
 */

namespace Dev\LogData\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToggleXdebugCommand extends Command
{
    protected function configure()
    {
        $this->setName('dev:xdebug:toggle');
        $this->setDescription('Toggles xdebug on or off');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xdebugIni = file_get_contents('/etc/php/7.1/mods-available/xdebug.ini');
        preg_match('/^;z/', $xdebugIni, $matches);
        $result = $matches[0] ?? false;

        if ($result) {
            $out = preg_replace('/^;z/', 'z', $xdebugIni);
            $output->writeln('Setting Xdebug ON');
        } else {
            $out = preg_replace('/^z/', ';z', $xdebugIni);
            $output->writeln('Setting Xdebug OFF');
        }
        file_put_contents('/etc/php/7.1/mods-available/xdebug.ini', $out);
        shell_exec('sudo service php7.1-fpm restart');
    }
}
